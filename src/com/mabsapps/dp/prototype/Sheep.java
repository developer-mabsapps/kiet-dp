package com.mabsapps.dp.prototype;

public class Sheep implements Animal{
    public Sheep(){
        System.out.println("Sheep Constructor: Sheep Created");
    }

    @Override
    public Animal makeCopy() {
        System.out.println("Sheep Cloning Started");

        Sheep obj = null;

        try{
            obj = (Sheep) super.clone();

        }catch (CloneNotSupportedException cnse){
            System.out.println("Can't be cloned: "+cnse.toString());
        }
        return obj;
    }

}
