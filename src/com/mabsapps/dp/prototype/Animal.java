package com.mabsapps.dp.prototype;

public interface Animal extends Cloneable{
    Animal makeCopy();
}
