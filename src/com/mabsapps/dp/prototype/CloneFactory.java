package com.mabsapps.dp.prototype;

public class CloneFactory {
    public Animal getClone(Animal sample){
        return sample.makeCopy();
    }
}
