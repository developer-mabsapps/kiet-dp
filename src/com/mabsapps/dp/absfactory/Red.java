package com.mabsapps.dp.absfactory;

public class Red implements Color{
    @Override
    public void fill() {
        System.out.println("Red::Fill()");
    }
}
