package com.mabsapps.dp.absfactory;

public interface Shape {
    void draw();
}
