package com.mabsapps.dp.absfactory;

public class Blue implements Color{
    @Override
    public void fill() {
        System.out.println("Blue::fill()");
    }
}
