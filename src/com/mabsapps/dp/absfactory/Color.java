package com.mabsapps.dp.absfactory;

public interface Color {
    public void fill();
}
