package com.mabsapps.dp.absfactory;

public class FactoryProducer {

    public static AbstractFactory getFactory(String factoryType){
        if(factoryType == null){
            return null;
        }
        if(factoryType.equalsIgnoreCase("color")){
            return new ColorFactory();
        }else
            if(factoryType.equalsIgnoreCase("shape")){
                return new ShapeFactory();
            }

        return null;
    }
}
