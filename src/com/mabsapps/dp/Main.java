package com.mabsapps.dp;

//Imports for Abstract Factory Demo
import com.mabsapps.dp.absfactory.AbstractFactory;
import com.mabsapps.dp.absfactory.Color;
import com.mabsapps.dp.absfactory.FactoryProducer;
import com.mabsapps.dp.absfactory.Shape;
import com.mabsapps.dp.builder.OldRobotBuilder;
import com.mabsapps.dp.builder.Robot;
import com.mabsapps.dp.builder.RobotBuilder;
import com.mabsapps.dp.builder.RobotEngineer;
import com.mabsapps.dp.chain.*;
import com.mabsapps.dp.observer.GetTheStock;
import com.mabsapps.dp.observer.StockGrabber;
import com.mabsapps.dp.observer.StockObserver;
import com.mabsapps.dp.observerclass.PTVWeather;
import com.mabsapps.dp.observerclass.WeatherStation;
import com.mabsapps.dp.oop.Person;

//////Imports for Factory Demo
//import com.mabsapps.dp.factory.Shape;
//import com.mabsapps.dp.factory.ShapeFactory;

//Imports for Prototype Demo
import com.mabsapps.dp.prototype.CloneFactory;
import com.mabsapps.dp.prototype.Sheep;

public class Main {

    public static void main(String[] args) {

        //factoryDemo();
        //abstractFactoryDemo();
        //prototypeDemo();
        //builderDemo();
        //        observerDemo();
        //chainOfResponsibilityDemo();

        observerClassDemo();

    }
    public static void observerClassDemo(){
        WeatherStation station = new WeatherStation();
        PTVWeather observerOne = new PTVWeather(station);

        station.add(observerOne);

        station.setData(34);
        station.setData(45);
    }

    private static void observerDemo(){
        // Create the Subject object
        // It will handle updating all observers
        // as well as deleting and adding them

        StockGrabber stockGrabber = new StockGrabber();

        // Create an Observer that will be sent updates from Subject

        StockObserver observer1 = new StockObserver(stockGrabber);

        stockGrabber.setIBMPrice(197.00);
        stockGrabber.setAAPLPrice(677.60);
        stockGrabber.setGOOGPrice(676.40);

        StockObserver observer2 = new StockObserver(stockGrabber);

        stockGrabber.setIBMPrice(197.00);
        stockGrabber.setAAPLPrice(677.60);
        stockGrabber.setGOOGPrice(676.40);

        // Delete one of the observers

        // stockGrabber.unregister(observer2);

        stockGrabber.setIBMPrice(197.00);
        stockGrabber.setAAPLPrice(677.60);
        stockGrabber.setGOOGPrice(676.40);

        // Create 3 threads using the Runnable interface
        // GetTheStock implements Runnable, so it doesn't waste
        // its one extendable class option

        Runnable getIBM = new GetTheStock(stockGrabber, 2, "IBM", 197.00);
        Runnable getAAPL = new GetTheStock(stockGrabber, 2, "AAPL", 677.60);
        Runnable getGOOG = new GetTheStock(stockGrabber, 2, "GOOG", 676.40);

        // Call for the code in run to execute

        new Thread(getIBM).start();
        new Thread(getAAPL).start();
        new Thread(getGOOG).start();


    }
    private static void chainOfResponsibilityDemo(){
        // Here I define all of the objects in the chain

        Chain chainCalc1 = new AddNumbers();
        Chain chainCalc2 = new SubtractNumbers();
        Chain chainCalc3 = new MultNumbers();
        Chain chainCalc4 = new DivideNumbers();

        // Here I tell each object where to forward the
        // data if it can't process the request

        chainCalc1.setNextChain(chainCalc2);
        chainCalc2.setNextChain(chainCalc3);
        chainCalc3.setNextChain(chainCalc4);

        // Define the data in the Numbers Object
        // and send it to the first Object in the chain

        Numbers request = new Numbers(4,2,"sub");

        chainCalc1.calculate(request);


    }



    private static void builderDemo(){
        // Get a RobotBuilder of type OldRobotBuilder

        RobotBuilder oldStyleRobot = new OldRobotBuilder();

        // Pass the OldRobotBuilder specification to the engineer

        RobotEngineer robotEngineer = new RobotEngineer(oldStyleRobot);

        // Tell the engineer to make the Robot using the specifications
        // of the OldRobotBuilder class

        robotEngineer.makeRobot();

        // The engineer returns the right robot based off of the spec
        // sent to it on line 11

        Robot firstRobot = robotEngineer.getRobot();

        System.out.println("Robot Built");

        System.out.println("Robot Head Type: " + firstRobot.getRobotHead());

        System.out.println("Robot Torso Type: " + firstRobot.getRobotTorso());

        System.out.println("Robot Arm Type: " + firstRobot.getRobotArms());

        System.out.println("Robot Leg Type: " + firstRobot.getRobotLegs());

    }


//    private static void prototypeDemo() {
//        CloneFactory animalFactory = new CloneFactory();
//
//        Sheep sampleSheep = new Sheep();
//        Sheep onlyRefer = sampleSheep;
//
//        Sheep clonedSheep = (Sheep) animalFactory.getClone(sampleSheep);
//
//        System.out.println(sampleSheep);
//        System.out.println(clonedSheep);
//        System.out.println(onlyRefer);
//        System.out.println("Sample Hashcode: "+System.identityHashCode(sampleSheep));
//        System.out.println("Cloned Hashcode: "+System.identityHashCode(clonedSheep));
//        System.out.println("Ref Hashcode: "+System.identityHashCode(onlyRefer));
//    }


    private static void abstractFactoryDemo() {
        AbstractFactory shapeFactory = FactoryProducer.getFactory("shape");
        AbstractFactory colorFactory = FactoryProducer.getFactory("color");

        Shape shape = shapeFactory.getShape("circle");
        shape.draw();
        shape = shapeFactory.getShape("square");
        shape.draw();

        Color color = colorFactory.getColor("blue");
        color.fill();

    }

//    private static void factoryDemo(){
//        ShapeFactory factory = new ShapeFactory();
//
//        Shape shape;
//
//        shape = factory.getShape("square");
//        shape.draw();
//
//        shape = factory.getShape("circle");
//        shape.draw();
//        //Main
//    }
}
