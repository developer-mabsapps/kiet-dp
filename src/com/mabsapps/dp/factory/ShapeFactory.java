package com.mabsapps.dp.factory;

public class ShapeFactory {
    public Shape getShape(String shape){
        if(shape == null)
            return null;
        if(shape.equalsIgnoreCase("square")){
            return new Square();
        }else
            if(shape.equalsIgnoreCase("circle")){
                return new Circle();
            }

        return null;

    }
}
