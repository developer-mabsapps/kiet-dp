package com.mabsapps.dp.factory;

public interface Shape {
    void draw();
}
