package com.mabsapps.dp.observerclass;

import java.util.ArrayList;
import java.util.List;

public class WeatherStation implements IObservable{

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
        notifyObservers();
    }

    int data;

    List<IObserver> list = new ArrayList<IObserver>();

    @Override
    public void add(IObserver observer) {
        list.add(observer);
    }

    @Override
    public void del(IObserver observer) {
        list.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (IObserver current: list) {
            current.update();
        }
    }
}
