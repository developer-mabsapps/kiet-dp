package com.mabsapps.dp.observerclass;

public interface IObservable {

    public void add(IObserver observer);
    public void del(IObserver observer);
    public void notifyObservers();

}
