package com.mabsapps.dp.observerclass;

public interface IObserver {
    public void update();
}
