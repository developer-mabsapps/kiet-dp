package com.mabsapps.dp.observerclass;

public interface IDisplayable {
    public void display(String value);
}
