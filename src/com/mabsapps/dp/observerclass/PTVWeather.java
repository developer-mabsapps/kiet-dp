package com.mabsapps.dp.observerclass;

public class PTVWeather implements IObserver, IDisplayable{
    WeatherStation observerable;

    public PTVWeather(WeatherStation object){
        this.observerable = object;
    }
    @Override
    public void update() {
        //this.observerable.getData();
        display("Current Temperature is "+this.observerable.getData());
    }


    @Override
    public void display(String value) {
        System.out.println(value);
    }
}
